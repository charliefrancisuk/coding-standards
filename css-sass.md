## CSS / SASS

### Introduction

Every project should be coded as if you are trying to keep your future self from murdering you; and with this comes three rules that should be abided by:

1. Keep stylesheets maintainable.
2. Keep code readable and sane.
3. Keep stylesheets scalable.

These rules will:

1. Set a good standard for code quality.
2. Promote consistency across projects.
3. Increase familiarity across projects.
4. Increase productivity.

The following sections contain a number of recommendations and approaches that will help in applying these rules.

### Syntax and Formatting

Code that looks clean, feels clean, while ugly code allows for mistakes and poor upkeep due to it's already messy nature.

#### Class Naming

Class names should follow the BEM methodology (block, element, modifier). This will give your CSS classes more transparency and meaning to other developers.

* Blocks represent the higher level of an abstraction or component (e.g. `.media`).  
* Elements represent a descendent of a block that helps form the block as a whole (e.g. `.media__img`).  
* Modifiers represent a different state or version of the block or element (e.g. `.media__img--large`).

The reason for double rather than single hyphens and underscores is so that your block itself can be hyphen delimited, for example `.site-logo--small`.

A simple set of rules that will help follow this methodoloty are to:

* Keep classes lowercase and use dashes instead of underscores or camelCase. Dashes serve as natural breaks in related class (.btn and .btn-danger).
* Avoid excessive and arbitrary shorthand notation. .btn is useful for button, but .s doesn't mean anything.
* Keep class names as short and succinct as possible.
* Use meaningful names; use structural or purposeful names over presentational.
* Prefix classes based on the closest parent or base class.
* Use .js-* classes to denote behavior (as opposed to style), but keep these classes out of your CSS.

A more in-depth article on BEM can be found over at [CSS Wizardry](http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/)

**Example**

```css
.site-search {} /* Block */
.site-search__field {} /* Element */
.site-search--full {} /* Modifier */
```

#### Syntax and Spacing

* Use soft-tabs of a four (4) space indent.
* When grouping selectors, place each one on a new line.
* Place our opening brace `{` on the same line as out last selector.
* Put a space before our opening brace `{`.
* Put a space after `:` in property declarations.
* Place properties and values on the same line.
* Put a trailing semi-colon `;` on our last declaration.
* Each declaration should appear on its own line for more accurate error reporting.
* Comma-separated property values should include a space after each comma.
* Place closing braces `}` of declaration blocks on a new line.

#### Formatting

* Use full hex colour codes with uppercase characters `#FF0000` unless using `rgba()`.
* Use `//` for comment blocks (instead of `/* */`).
* Avoid specifying units for zero values, e.g., `margin: 0;` instead of `margin: 0px;`.
* When using decimal values below 1, leave of the prepending 0, e.g. `.2` instead of `0.2`.
* Strive to limit use of shorthand declarations to instances where you must explicitly set all the available values.
* Vendor prefixed properties should be indented so that the declaration's value lines up vertically.
* Place media queries as close to their relevant rule sets whenever possible. Don't bundle them all in a separate stylesheet or at the end of the document. 

#### Declaration Order

Related property declarations should be grouped together following the order:

1. Positioning
2. Box model
3. Typographic
4. Visual

```css
.declaration-order {
    /* Positioning */
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 100;

    /* Box-model */
    display: block;
    float: right;
    width: 100px;
    height: 100px;

    /* Typography */
    font: normal 13px "Helvetica Neue", sans-serif;
    line-height: 1.5;
    color: #333;
    text-align: center;

    /* Visual */
    background-color: #f5f5f5;
    border: 1px solid #e5e5e5;
    border-radius: 3px;

    /* Misc */
    opacity: 1;
}
```

For a complete list of properties and their order, please see [Recess](https://github.com/twitter/recess/blob/master/lib/lint/strict-property-order.js).

#### Misc

* As a rule of thumb, nesting in SCSS should be avoided wherever possible to avoid complex specificity issues.
* Use whitespace meaningfully, keep space between rules especially when nesting.

#### Examples

```scss
// Example of good basic formatting practices
.foo,
.foo--bar,
.baz {
    display: block;
    background-color: #000000;
    color: #FFFFFF;
    opacity: .5;

    .foo__item {
        padding-left: 10px;
    }
}

// Avoid unnecessary shorthand declarations
.not-so-good {
    margin: 0 0 20px;
}
.good {
    margin-bottom: 20px;
}

// Good Browser prefix order and alignment
.browser-prefix {
    -webkit-box-shadow: inset 0 1px 5px rgba(0, 0, 0, 0.2);
       -moz-box-shadow: inset 0 1px 5px rgba(0, 0, 0, 0.2);
        -ms-box-shadow: inset 0 1px 5px rgba(0, 0, 0, 0.2);
         -o-box-shadow: inset 0 1px 5px rgba(0, 0, 0, 0.2);
            box-shadow: inset 0 1px 5px rgba(0, 0, 0, 0.2);
}
```

#### Preprocessors

Grunt?

Gulp?