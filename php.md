## PHP

### Introduction

Every project should be coded as if you are trying to keep your future self from murdering you; and with this comes three rules that should be abided by:

1. Keep code maintainable.
2. Keep code readable
3. Keep code sane.

These rules will:

1. Set a good standard for code quality.
2. Promote consistency across projects.
3. Increase familiarity across projects.
4. Increase productivity.

The following sections contain a number of recommendations and approaches that will help in applying these rules.

### Syntax and Formatting