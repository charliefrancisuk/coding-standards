# PROJECT NAME

Brief description/introduction to the product.


## Authors

### Project Managers

* Full Name (email@address.com)

### Developers

* Full Name (email@address.com)


## Table of Contents

* [Requirements](#requirements)
* [Environments](#environments)
    * [Development](#environments-development)
    * [Production](#environments-production)
* [Installation](#installation)
* [Deployment](#deployment)
* [Security](#security)
* [Analytics Tracking](#analytics-tracking)


## Requirements

* PHP 5.5
* Java
* MySQL
* PHP mCrypt extension
* APC Cache
* Redis
* Composer
* Bower
* Magnolia
* Laravel
* Wordpress 4.1
* HTML 5
* CSS 3
* jQuery 2.1


## Environments

Description of environments (AWS, Rackspace, Twitterbot etc).

### Development

Development environment notes.

#### Servers

    Server 01: XXX.XXX.XXX.XXX
    user: xxxx
    pass: xxxx
    key: xxxx

### Production

Production environment notes.

#### Servers

    Server 01: XXX.XXX.XXX.XXX
    user: xxxx
    pass: xxxx
    key: xxxx


## Installation

Instructions on how to install and setup the project on a development machine. (composer, bower, grunt, vagrant, db, imports etc).


## Deployment

Instructions on how to deploy the project onto a server (development/production). (capistrano, jenkins, git pull, server replication etc).


## Libraries

A list of libraries and plugins that are used (js libraries, composer packages, npm modules etc).


## Security

Additional security information (encrypted forms, SSL certificate information).


## Analytics Tracking

Instructions, account details and information on analytics/data tracking.