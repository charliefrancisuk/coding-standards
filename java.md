## Maven

Download [Apache Maven](http://maven.apache.org/download.html)

```ssh
sudo cd ~/Downloads
sudo unzip apache-maven-3.2.5-bin.zip
sudo mkdir /usr/local/apache-maven
sudo mv apache-maven-3.2.5-bin /usr/local/apache-maven/
export PATH=$PATH:/usr/local/apache-maven/apache-maven-3.2.5/bin
```

Update settings.xml `~/.m2/settings.xml`

## Java JDK

Download [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) and run the install DMG

## Install IntelliJ

Download [IntelliJ Ultimate Edition](https://www.jetbrains.com/idea/download/) and run the install DMG
