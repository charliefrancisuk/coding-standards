# Coding Standards

This document contains a list of reasonable guidelines that should be encouraged for every project, current and future.

Editors and IDE's should have the following settings enabled to guarantee code renders the same in any environment.

* Soft tabs of four (4) spaces.
* Trim trailing white space on save.
* Set encoding to UTF-8.
* Add new line at end of files.

**N.B.** This document is a work in progress and should be reviewed and updated when new standards, technologies or frameworks are brought into projects.

## Frontend

* HTML
* [CSS / SASS](css-sass.md)
* Grunt
* Gulp
* JavaScript

## Backend

* PHP
* Java

## Other

* Git
* Jenkins
* AWS
* Rackspace